#!/bin/bash
set -e

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [[ $machine = *Linux* ]]; then
    apt-get install jq
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys CC86BB64
    add-apt-repository ppa:rmescandon/yq
    apt update
    apt install yq -y
fi

SERVICE_EXIST=false
file="./deploy/lambda/.env-lambda"
while IFS='=' read -r key value
do
  if [[ ! -s ${key} ]] && [[ ! ${key} = "API_DATALOGGER" ]] && [[ ! ${key} = "API_DATALOGGER_MODE" ]] && [[ ! ${key} =~ "#" ]] && [[ ! ${value} = "" ]] && [[ ! ${value} = "null" ]]
  then 
    if [[ ${key} = "SERVICE_NAME" ]]
    then
      SERVICE_EXIST=${value}
    fi
    if [[ $value =~ SECRET_ ]]; then
        value=$(aws secretsmanager get-secret-value --secret-id lambda-prod | jq --raw-output '.SecretString' | jq -r .${value})

        if [[ ${value} = "" ]] || [[ ${value} = "null" ]] || [[ -s ${value} ]]; then
          echo "ERROR: Null value for ENV variable: ${key}"
          exit 1
        fi
    fi

    yq w -i solution.yaml Resources.rLambda.Properties.Environment.Variables.${key} ${value}
  fi
done < "$file"

echo "Service name ${SERVICE_EXIST}"
if [[ ${SERVICE_EXIST} = "false" ]]
then
  echo "ERROR: SERVICE_NAME is not exist in the .env-lambda variable."
  exit 1
fi
