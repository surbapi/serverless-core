#!/bin/bash
set -e

FILES=solution.yaml,pipeline.yaml,secret_replacement.sh,build.sh,auth-dist.json

if [[ $1 = *get* ]]; then
    bash serverless-core.sh remove
    USERNAME=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .USERNAME)
    PASSWORD=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .PASSWORD)

    IFS=$',' read -ra FILE <<< "${FILES}"
    for i in "${FILE[@]}"; do
        wget https://${USERNAME}:${PASSWORD}@bitbucket.org/surbapi/serverless-core/raw/master/${i}
    done

    if [ -f "solution-custom.yaml" ]; then
        cp solution-custom.yaml solution.yaml
    fi

    bash secret_replacement.sh
fi

if [[ $1 = *remove* ]]; then
    IFS=$',' read -ra FILE <<< "${FILES}"
    for i in "${FILE[@]}"; do
        if [ -f "$i" ]; then
            rm ${i}*
        fi
    done
fi