#!/bin/bash
FILES=Makefile,serverless-core-node-lambda-scheduler.sh

USERNAME=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .USERNAME)
PASSWORD=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .PASSWORD)

IFS=$',' read -ra FILE <<< "${FILES}"
for i in "${FILE[@]}"; do
    if [ -f "$i" ]; then
        rm ${i}*
    fi
    wget https://${USERNAME}:${PASSWORD}@bitbucket.org/surbapi/serverless-core/raw/master/${i} --no-cache
done

mv serverless-core-node-lambda-scheduler.sh serverless-core.sh