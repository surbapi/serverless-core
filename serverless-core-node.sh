#!/bin/bash
set -e

FILES=solution-node.yaml,pipeline-node.yaml,secret_replacement.sh,build.sh,auth-dist.json

if [[ $1 = *get* ]]; then
    bash serverless-core.sh remove
    USERNAME=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .USERNAME)
    PASSWORD=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .PASSWORD)

    IFS=$',' read -ra FILE <<< "${FILES}"
    for i in "${FILE[@]}"; do
        wget https://${USERNAME}:${PASSWORD}@bitbucket.org/surbapi/serverless-core/raw/master/${i} --no-cache
    done

    if [ -f "solution-custom.yaml" ]; then
        cp solution-custom.yaml solution.yaml
    else
        mv solution-node.yaml solution.yaml
    fi

    mv pipeline-node.yaml pipeline.yaml

    bash secret_replacement.sh
fi

if [[ $1 = *remove* ]]; then
    IFS=$',' read -ra FILE <<< "${FILES}"
    for i in "${FILE[@]}"; do
        if [ -f "$i" ]; then
            rm ${i}*
        fi
    done

    if [ -f "solution.yaml" ]; then
        rm solution.yaml*
    fi

    if [ -f "pipeline.yaml" ]; then
        rm pipeline.yaml*
    fi
fi