set -ex

mkdir -p /root/.ssh &&  chmod 700 /root/.ssh &&  ssh-keyscan bitbucket.org > /root/.ssh/known_hosts

BASE64_PRIVATE_KEY=$(aws secretsmanager get-secret-value --secret-id lambda-prod | jq --raw-output '.SecretString' | jq -r .SECRET_BASE64_PRIVATE_KEY)
echo "$BASE64_PRIVATE_KEY" | base64 -d > /root/.ssh/id_rsa &&  chmod 700 /root/.ssh/id_rsa

printenv
ls -R
CURRENTDIR=`pwd`
mkdir -p /root/.ssh && chmod 777 /root/.ssh && ssh-keyscan bitbucket.org > /root/.ssh/known_hosts
wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add -
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install -y curl libssl-dev openssh-server openssh-client git libpng-dev zlib1g-dev libzip-dev libmcrypt-dev zip unzip software-properties-common

apt-get install libcurl3 -y
apt-get install libcurl4 libcurl4-openssl-dev -y

apt-get install -y build-essential apt-transport-https ca-certificates gnupg2 software-properties-common gcc g++ make git screen default-libmysqlclient-dev gnupg2 libjpeg-dev libjpeg-turbo-progs
# echo "deb https://packages.sury.org/php/ stretch main" | tee /etc/apt/sources.list.d/php.list
# echo "deb https://packages.sury.org/php/ buster main" > /etc/apt/sources.list.d/php.list
apt-get purge -y php7.2
apt-get install -y php7.2 zip unzip php7.2-dev php7.2-mbstring php7.2-mysql php7.2-pdo php7.2-curl php7.2-json php7.2-xmlrpc php7.2-zip php7.2-gd php-mongodb php-intl php7.2-bcmath php7.2-mcrypt php7.2-bz2 php7.2-xml
phpenmod -v php7.2 mongodb
update-alternatives --set php /usr/bin/php7.2
rm /root/.phpenv/shims/php
php -version
which php
which composer
composer clear-cache
composer self-update
composer clear-cache
composer install --ignore-platform-reqs -n  && composer dumpautoload -o
