# README

This README contain brief process of setting up the serverless environment for Jojonomic micro services.

## How do I get set up

The following steps need to be executed/copied from this repository into target codes in order:

- Create file `.env-lambda` contains the Lambda environment variable in the `deploy/lambda` directory of the target repository and follow below steps:
    - Secure any important credential in the AWS Secret Manager and note the variable name
    - Replace any variable value in the .env-lambda with the above stored name from the secret manage with `SECRET_` prefix
- Manually copy the file `init.sh` from this repository into the `deploy/lambda` directory of the target repository
- Enable Pipelines in the Bitbucket.org setting of the target repository and add the following repository variables:
    - `REPO_NAME` with value of the target repository name. Eq: `user-service`
    - `AWS_DEFAULT_REGION` with value `ap-southeast-1`
    - `AWS_KEY` with value from AWS ACCESS KEY that has access into the secret manager and S3 bucket upload
    - `AWS_SECRET` with value from AWS SECRET KEY that has access into the secret manager and S3 bucket upload

## How do I initiate the Serverless

After above steps followed and all necessary files copied into the target repository, follow the following steps:

- Setup the aws credential in the file `~/.aws/credentials` with example below:

```bash
[default]
aws_access_key_id = <get access key from the AWS console>
aws_secret_access_key = <get secret key from the AWS console>
```

- Execute `bash deploy/lambda/init.sh` from the target repository to get additional dynamic executable files to setup the environment
- Evaluate for any error that this script execution produce, rectify those error(s) accordinly (as directed)
- Successful execution of above command will result pipeline creation in the AWS CodePipeline accordingly
- Commit the code changes under `lambda_prod` branch to deploy the solution automatically into AWS Lambda

## Need help? Who do I talk to

Please contact the following person(s) to get any help:

- Iman Suherman with contact details:
    - <iman@jojonomic.com>
    - +61430058886 (Phone, Text, Facetime and WhatsApp)

- Aufar Sukmajaya with contact details:
    - <aufar@jojonomic.com> 
