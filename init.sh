#!/bin/bash
FILE=execute.sh

if [ -z "${AWS_PROFILE}" ]; then
    echo error AWS_PROFILE is undefined E.G export AWS_PROFILE=default
    exit 1
fi

USERNAME=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .USERNAME)
PASSWORD=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .PASSWORD)

if [ -f "${FILE}" ]; then
    rm ${FILE}*
fi
wget https://${USERNAME}:${PASSWORD}@bitbucket.org/surbapi/serverless-core/raw/master/${FILE} --no-cache

bash ${FILE}