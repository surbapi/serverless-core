#!/bin/bash

MONOLOG=`cat bootstrap/app.php| grep "use Monolog" | wc -l`
LOGGER=`cat bootstrap/app.php| grep "new Logger" | wc -l`
PREFIX=`cat bootstrap/app.php| grep "'prefix' => " | wc -l`
PREFIX_ASSIGNMENT=`cat bootstrap/app.php| grep "\$prefix = strtolower(env('SERVICE_NAME'));" | wc -l`
SENTRY_LUMEN=`cat bootstrap/app.php| grep "SentryLumenServiceProvider" | wc -l`
HEALTHZ=`cat routes/web.php| grep "'healthz" | wc -l`
LUMEN_START=`cat public/index.php| grep "'LUMEN_START" | wc -l`

CHECKPOINT=true
if [ ${MONOLOG} = 0 ]; then
    echo "ERROR: Missing 'use Monolog\Logger;' in the bootstrap/app.php"
    CHECKPOINT=false
fi

if [ ${LOGGER} = 0 ]; then
    echo "ERROR: Missing '\$log = new Logger('app');' in the bootstrap/app.php"
    CHECKPOINT=false
fi

if [ ${PREFIX} = 0 ]; then
    echo "ERROR: Missing \"'prefix' => \$prefix\" in the bootstrap/app.php"
    CHECKPOINT=false
fi

if [ ${PREFIX_ASSIGNMENT} = 0 ]; then
    echo "ERROR: Missing \"\$prefix = strtolower(env('SERVICE_NAME'));\" in the bootstrap/app.php"
    CHECKPOINT=false
fi

if [ ${SENTRY_LUMEN} != 0 ]; then
    echo "ERROR: Please remove \"\$app->register(Sentry\SentryLaravel\SentryLumenServiceProvider::class);\" from the bootstrap/app.php"
    CHECKPOINT=false
fi

if [ ${HEALTHZ} = 0 ]; then
    echo "ERROR: Missing \"\$router->get('healthz', function () { return 1; });\" in the routes/web.php"
    CHECKPOINT=false
fi

if [ ${LUMEN_START} = 0 ]; then
    echo "ERROR: Missing \"define('LUMEN_START', microtime(true));\" in the public/index.php"
    CHECKPOINT=false
fi

if [ ${CHECKPOINT} = "false" ]; then
    echo "Init process aborted! Please correct above issue(s)."
    exit 1
fi

FILES=Makefile,serverless-core.sh,bitbucket-pipelines.yml,JojoLambdaExceptionHandler.php

USERNAME=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .USERNAME)
PASSWORD=$(aws secretsmanager get-secret-value --secret-id bitbucket-auth | jq --raw-output '.SecretString' | jq -r .PASSWORD)

IFS=$',' read -ra FILE <<< "${FILES}"
for i in "${FILE[@]}"; do
    if [ -f "$i" ]; then
        rm ${i}*
    fi
    wget https://${USERNAME}:${PASSWORD}@bitbucket.org/jojocoders/serverless-core/raw/master/${i} --no-cache
done

mv JojoLambdaExceptionHandler.php app/Exceptions

IGNOREDS=Makefile,serverless-core.sh,execute.sh
IFS=$',' read -ra IGNORED <<< "${IGNOREDS}"
for i in "${IGNORED[@]}"; do
    IGNORE_CHECK=`cat .gitignore| grep ${i} | wc -l`

    if [ ${IGNORE_CHECK} = 0 ]; then
        if [ ${i} = Makefile ]; then
            echo "" >> .gitignore 
        fi
        echo ${i} >> .gitignore 
    fi
done

run_update() {
    if eval "$@"; then
        echo "Success"
    else
        echo "No pipeline update required"
        bash serverless-core.sh remove 
    fi
}

if [ -z "${REPO_NAME}" ]; then
    REPO_NAME=$(basename -s .git `git config --get remote.origin.url`)
fi

if [ -z "${REPO_NAME}" ]; then
    echo "ERROR: REPO_NAME is not defined in the Repository variables."
fi

STACK_EXIST=`aws cloudformation list-stacks | grep ${REPO_NAME}-prod-cicd-pipeline-cf | wc -l`

if [ ${STACK_EXIST} != 0 ]; then
    run_update "make update"
else
    make deploy
fi