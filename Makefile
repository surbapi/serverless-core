
.PHONY: test checkpkey checkenv checkconsul dep build run buildrun push checkvar env-vanilla env-consul env-fileconsul env-file env-recuring-fileconsul check-aws-profile check-aws-solution deploy update upload init init-update

export BRANCH_NAME		 ?= $(shell git rev-parse --abbrev-ref HEAD)
export REPO_NAME     	 ?= $(shell basename -s .git `git config --get remote.origin.url`)
export VERSION           ?= $(shell git show -q --format=%h)
export PORT              ?= 30001
export APP_STORAGE_PATH  ?=/var/www/html/storage/logs
#export HOST_ENV          ?= $(shell cat ./deploy/hosts_env)
#ENV mode = vanilla, consul, fileconsul, file
export ENV_MODE          ?=fileconsul

ifeq ($(LOGS_PATH),)
  VOLUME=
else
  VOLUME=-v $(LOGS_PATH)/$(REPO_NAME):$(APP_STORAGE_PATH)
endif

ifneq (,$(findstring _prod,$(BRANCH_NAME)))
    BRANCH=prod
endif

test:
	do unit test

dep:
	rm composer.lock
	rm -rf ./vendor
	composer install

checkpkey:
ifndef BASE64_PRIVATE_KEY
	$(error BASE64_PRIVATE_KEY must be set.)
endif

checkenv:
ifndef APP_ENV
	$(error APP_ENV must be set.)
endif

checkconsul:
ifndef CONSUL_ADDR
	$(error CONSUL_ADDR must be set.)
endif

build: checkpkey checkenv checkconsul
	-docker image rm $(REPO_NAME):$(VERSION) --force
	docker build -t $(REPO_NAME):$(VERSION) \
	--build-arg TARGET_ENV=$(APP_ENV) \
	--build-arg BASE64_PRIVATE_KEY="$(BASE64_PRIVATE_KEY)" \
	--build-arg REPO_NAME="$(REPO_NAME)" \
	--build-arg CONSUL_ADDR="$(CONSUL_ADDR)" \
	--build-arg ENV_MODE="$(ENV_MODE)" \
	-f ./deploy/Dockerfile .

run:
	-docker container rm $(REPO_NAME) --force
ifdef LOGS_PATH
	mkdir -p $(LOGS_PATH)/$(REPO_NAME)
endif
	docker run -d --name $(REPO_NAME) $(VOLUME) --expose=$(PORT) -p $(PORT):80 $(REPO_NAME):$(VERSION)
ifdef NETWORK
	docker network connect $(NETWORK) $(REPO_NAME)
	docker restart $(REPO_NAME)
endif
	echo "success"

buildrun: build run

push:
	docker push $(REPO_NAME):$(VERSION)

checkvar: #for debugging purposes
	echo $(REPO_NAME):$(VERSION):$(PORT):$(APP_ENV)
ifdef NETWORK
	echo $(NETWORK)
endif
ifdef LOGS_PATH
	echo $(LOGS_PATH)
endif

#example: echo "php -S 0.0.0.0:80 -t public" > cmd.sh
env-vanilla:
	echo "" >> .env
	echo "apache2-foreground" > cmd.sh

env-consul:
	echo "" >> .env
	echo "./envconsul -consul-addr $(CONSUL_ADDR) -prefix $(REPO_NAME) -once" > cmd.sh
	echo "apache2-foreground" >> cmd.sh

env-fileconsul:
	./envconsul -consul-addr $(CONSUL_ADDR) -prefix $(REPO_NAME) -once -pristine env > .env
	echo "apache2-foreground" > cmd.sh

env-recuring-fileconsul:
	echo "./envconsul -consul-addr $(CONSUL_ADDR) -prefix $(REPO_NAME) -once -pristine env > .env" > cmd.sh
	echo "apache2-foreground" >> cmd.sh

env-file:
	echo "" >> .env
	echo "apache2-foreground" > cmd.sh

check-aws-profile:
ifndef AWS_PROFILE
	$(error AWS_PROFILE is undefined E.G export AWS_PROFILE=default)
endif

upload:
ifndef BRANCH
	$(invalid branch for deployment.)
else 
	bash serverless-core.sh get
	zip -r $(REPO_NAME).zip * -x '*cache*' -x '*vendor*'
	aws s3 cp $(REPO_NAME).zip s3://jojo-repositories/$(BRANCH)/$(REPO_NAME).zip
	rm $(REPO_NAME).zip 
	bash serverless-core.sh remove
endif

deploy: check-aws-profile  
ifndef BRANCH
	$(invalid branch for deployment.)
else 
	bash serverless-core.sh get
	aws cloudformation create-stack \
	--stack-name ${REPO_NAME}-${BRANCH}-cicd-pipeline-cf \
	--template-body file://pipeline.yaml \
	--parameters ParameterKey=pSolutionNaming,ParameterValue=$(REPO_NAME) \
	ParameterKey=pProjectFile,ParameterValue=$(REPO_NAME).zip \
	ParameterKey=pBranch,ParameterValue=$(BRANCH) \
	--capabilities CAPABILITY_NAMED_IAM --region ap-southeast-1 --profile $(AWS_PROFILE)
	bash serverless-core.sh remove
endif

update: check-aws-profile
ifndef BRANCH
	$(invalid branch for update.)
else 
	bash serverless-core.sh get
	aws cloudformation update-stack \
	--stack-name ${REPO_NAME}-${BRANCH}-cicd-pipeline-cf \
	--template-body file://pipeline.yaml \
	--parameters ParameterKey=pSolutionNaming,ParameterValue=$(REPO_NAME) \
	ParameterKey=pProjectFile,ParameterValue=$(REPO_NAME).zip \
	ParameterKey=pBranch,ParameterValue=$(BRANCH) \
	--capabilities CAPABILITY_NAMED_IAM --region ap-southeast-1 --profile $(AWS_PROFILE)
	bash serverless-core.sh remove
endif

base: check-aws-profile  
	aws cloudformation create-stack \
	--stack-name jojonomic-bucket-storage-cf \
	--template-body file://base-pipeline.yaml \
	--capabilities CAPABILITY_NAMED_IAM --region ap-southeast-1 --profile $(AWS_PROFILE)

base-update: check-aws-profile  
	aws cloudformation update-stack \
	--stack-name jojonomic-bucket-storage-cf \
	--template-body file://base-pipeline.yaml \
	--capabilities CAPABILITY_NAMED_IAM --region ap-southeast-1 --profile $(AWS_PROFILE)
